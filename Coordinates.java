package com.company;

public class Coordinates {

    int x, y;
    Event e = null;

    /**
     * Constructor sets coordinates as incoming values, as well as checking if the incoming
     * coordinates are within the bounds of the worldMap.
     * @param x is x coordinate.
     * @param y is y coordinate.
     */
    Coordinates(int x, int y) {
        if(x < (-10) || x > 10 || y < (-10) || y > 10) {
            throw new IllegalArgumentException("Coordinates out of bounds!");
        }
        this.x = x;
        this.y = y;
    }

    /**
     * Checks if coordinates are the same as the specified coordinates.
     * @param xy coordinate that you are comparing to.
     * @return result of comparison.
     */
    boolean equals(Coordinates xy){
        return (this.x == xy.x && this.y == xy.y);
     }
}
