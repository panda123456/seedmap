package com.company;
import java.util.HashSet;
import java.util.Set;

class Event {
     String id;
     private Coordinates coordinates;
     Set<Integer> tickets = new HashSet<>();
     int potentialDistance;

    /**
     * Constructor sets the value as the incoming events
     * @param id unique identifier
     * @param coordinates event location
     * @param tickets set of ticket prices for event
     */
    Event(String id, Coordinates coordinates, int distance, Set<Integer> tickets){
        this.id = id;
        this.coordinates = coordinates;
        this.tickets = tickets;
        this.potentialDistance = distance;
    }
}
