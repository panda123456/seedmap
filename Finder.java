package com.company;

import java.util.*;

class Finder {

    private Map<Coordinates, Event> world;
    private TreeMap<Integer, Event> tm = new TreeMap<>();
    private int numberOfClosestEvents = 5;

    /**
     * For each coordinate that has an event, it calculates the distance from the coordinate and
     * sets that as a key for a new Treemap where the events are sorted in ascending distance from
     * given coordinate
     * @param world This is the randomly generated seed map
     */
    Finder(Map<Coordinates, Event> world){
        this.world = world;

        for(Coordinates check : world.keySet()){
            Event e = world.get(check);
            if(e.potentialDistance < 0) throw new NullPointerException("dist cannot be negative");
            if(!e.tickets.isEmpty()) tm.put(e.potentialDistance, world.get(check));
        }
    }

    /**
     * Simply a function to print out the desired format
     */
    void printClosest(){
        Iterator<Integer> it = tm.keySet().iterator();
        int i = numberOfClosestEvents;
        while(i > 0){
            Event e = tm.get(it.next());
            System.out.print("Event: " + e.id + " - ");
            System.out.print("Price: £" + getCheapestTicket(e) + ", " );
            System.out.println("Distance " + e.potentialDistance);
            i--;
        }
    }

    /**
     * Function to get the cheapest priced ticket for an event
     * @param e the event being checked
     * @return int of the cheapest priced ticket
     */
    private int getCheapestTicket(Event e){
        int cheapest = 9999;
        for(int x : e.tickets){
            if(cheapest > x) cheapest = x;
        }
        return cheapest;
    }
}
