package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Please input two integers separated by a space, within the bounds ");
        System.out.println("of -10 to 10 on the x and y axis.");
        System.out.println("The first number will be the x coordinate, ");
        System.out.print("the second will be the y coordinate.");
        System.out.println("For example: 3 12 will be (3,12).");

        System.out.print("First enter the x value here: ");
        int x = scanner.nextInt();
        System.out.println();
        System.out.print("Thanks you, now enter the y value: ");
        int y = scanner.nextInt();

        scanner.close();

        //System.out.println("x: " + x + " y: " + y);

        int numTicketPerEvent = 10;

        Coordinates xy = new Coordinates(x, y);
        World world = new World(numTicketPerEvent, xy);

        Finder finder = new Finder(world.worldMap);
        finder.printClosest();
    }
}
