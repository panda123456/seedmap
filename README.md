# README #

### What is this repository for? ###

Used to allow Viagogo to review code.

### How do I get set up? ###

Download the java files into a folder, run javac over each file to compile and then run java on main to begin the program.

### How would you change your code if more than one event could be held at each Coordinate ###

I would have a hashset in my Coordinate class that would hold the events held there. The id of the event would be used as the key.

### How would you change your program if you were working with a much larger world
size? #

If the world were to get giant, then the world would have to be stored as a data base, with the coordinates being the primary key. 
