package com.company;

import java.util.*;

class World {
    Map<Coordinates, Event> worldMap = new HashMap<>();
    private Random random = new Random();
    private int maxNumberOfTicketPerEvent;
    private int percentageOfBoardWithoutEvents = 90;

    /**
     * Constructor to create the new worldMap map.
     * Increment through all 400 coordinates in the 20x20 worldMap.
     * At each coordinate, random function run which will return true about 30% of the time
     * but it is unknown when.
     * If random function does return true, Event created using createEvent function and put into
     * map with the coordinate of respective event as the key.
     * @param maxNumberOfTicketPerEvent set maximum number of tickets an event can have.
     */
    World(int maxNumberOfTicketPerEvent, Coordinates input){
        this.maxNumberOfTicketPerEvent = maxNumberOfTicketPerEvent;
        for(int i = (-10); i <= 10; i++){
            for(int j = (-10); j <= 10; j++) {
                if(randomAlgorithm()) {
                    Event event = createEvent(i, j, input);
                    Coordinates xy = new Coordinates(i,j);
                    worldMap.put(xy, event);
                }
            }
        }
    }

    //Decide whether to create event at that coordinate
    private boolean randomAlgorithm(){
        //System.out.println("randomAlgorith");
        int d = (int) (random.nextDouble()*100);
        return (d > percentageOfBoardWithoutEvents);
    }

    /**
     * Method to create event with random tickets
     * @param x coordinate
     * @param y coordinate
     * @return Event at coordinate (x,y) with Unique id and random set of ticket prices
     */
    private Event createEvent(int x, int y, Coordinates input){
        //System.out.println("createEvent");
        String id = "x" + String.valueOf(x) + "y" + String.valueOf(y);

        Coordinates cood = new Coordinates(x, y);
        Set<Integer> tickets = new HashSet<>();
        int maxTickets = (int) (random.nextDouble()*maxNumberOfTicketPerEvent);

        for(int i = 0; i < maxTickets; i++){
            int price = (int) (random.nextDouble()*1000);
            if(price < 0) throw new IllegalArgumentException("Price cannot be negative!");
            tickets.add(price);
        }

        int dist = getDist(cood, input);

        return new Event(id, cood, dist, tickets);
    }

    private int getDist(Coordinates xy, Coordinates check){
        // if(xy.equals(check)) return 0;
        return (Math.abs(xy.x - check.x) + Math.abs(xy.y - check.y));
    }
}
